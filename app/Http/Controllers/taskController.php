<?php

namespace App\Http\Controllers;

use App\Models\task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SendNotification;
class taskController extends Controller
{
    public function index(Request $request){
       
        $tasks = task::all();
        return view('task.index',compact('tasks'));
    }
    public function show($id){
        $task = task::find($id);
        return view('task.show',compact('task'));
    }
    public function create(){
        return view('task.create');
    }
    public function store(Request $request){
        $validate = Validator::make($request->all(),[
            'task' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'status' => 'required',
        ]);

        $status = "created";
        if($validate->passes()){
            $task = task::create([
                'task' => $request->task,
                'start_time' => $request->start_time,
                'end_time' => $request->end_time,
                'status' => $request->status,
            ]);   
            Session::flash('success','Task added successfully');
            
            return response()->json([
                'status' => true,
                'message' => 'Task added successfully !',
                Notification::send($task,new SendNotification($status)),
            ]);
        } else {
            return response()->json([
                'status' => false,
                'errors' => $validate->errors(),
            ]);
        }
    }
    public function edit($id)
    {
        $task = task::find($id);
        return view('task.edit',compact('task'));
    }
    public function update(Request $request,$id){
        $validate = validator::make($request->all(),[
            'task' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'status' => 'required',
        ]);

        $task = task::find($id);
        $status = "updated";

        if($validate->passes()){
            $task->task = $request->task;
            $task->start_time = $request->start_time;
            $task->end_time = $request->end_time;
            $task->status = $request->status;
            $task->update();
            
            Session::flash('inform','Task updated successfully');
            
            return response()->json([
                'status' => true,
                'message' => 'Task updated successfully',
                Notification::send($task,new SendNotification($status)),
            ]);
        } else {
            return response()->json([
                'status' => false,
                'errors' => $validate->errors()
            ]);
        }
    }
    public function delete($id){
        $task = task::find($id);

        if($task != null){
            
            $task->delete();

            Session::flash('danger','Task deleted successfully');
            
            $status = "deleted";

            Notification::send($task,new SendNotification($status));

            return response()->json([
                'status' => true,
                'message' => 'task deleted successfully'
            ]);
            
        } else {
            return response()->json([
                'status' => false,
                'message' => 'task is not found'
            ]);
        }
    }
}
