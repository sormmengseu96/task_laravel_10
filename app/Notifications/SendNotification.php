<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
// use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramMessage;

class SendNotification extends Notification
{
    use Queueable;

    public $status;
    /**
     * Create a new notification instance.
     */
    public function __construct($status)
    {
        $this->status = $status;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via($notifiable)
    {
        return ['telegram'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toTelegram($notifiable)
    {
        try{
            switch($notifiable['status']){
                case $notifiable['status'] == 1 :
                    $msg_status = 'Waiting';
                    break; 
                case $notifiable['status'] == 2 :
                    $msg_status = 'Processing';
                    break; 
                case $notifiable['status'] == 3 :
                    $msg_status = 'Complete';
                    break; 
                case $notifiable['status'] == 4 :
                    $msg_status = 'Disable';
                    break; 
            }

            $data = "============================"."\n"
                   .": 🌿 TASK : ".$this->status."\n"
                   ."============================"."\n"
                   .': 🆔 ID :' .$notifiable['id']."\n"
                   .': 📒 Task :' .$notifiable['task']."\n"
                   .': 🕔 Start time :' .$notifiable['start_time']."\n"
                   .': 🕦 End time :' .$notifiable['end_time']."\n"
                   .': 📅 Create at :' .$notifiable['created_at']."\n"
                   .': ⚙️ Status :' .$msg_status."\n";
            return TelegramMessage::create()
              ->to('-1002109118870')
              ->content($data);

        } catch (\Exception $e){
            return TelegramMessage::create()
              ->to('-1002109118870')
              ->content($e);
        }
     
           // echo $notifiable;
        // echo $id;
        // $message = $notifiable['task'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            
        ];
    }
}
