<?php

use App\Http\Controllers\taskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/',[taskController::class,'index' ])->name('task.index');
Route::get('/task/{id}/show',[taskController::class,'show' ])->name('task.show');
Route::get('/task/create',[taskController::class,'create' ])->name('task.create');
Route::post('/task/store',[taskController::class,'store' ])->name('task.store');
Route::get('/task/{id}/edit',[taskController::class,'edit' ])->name('task.edit');
Route::post('/task/{id}/update',[taskController::class,'update' ])->name('task.update');
Route::get('/task/{id}/delete',[taskController::class,'delete' ])->name('task.delete');