@extends('layout.app')

@section('content')
    <div class="container">
        @include('layout.message')
        <div class="card">
            <div class="card-header">
                <a href="{{ route('task.create') }}" class="btn btn-dark btn-sm">
                    <i class="bi bi-journal-plus"></i>
                </a>
            </div>
            <div class="card-body">
                <table id="example" class="table table-striped" style="width:100%;">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>TASK</th>
                            <th>START TIME</th>
                            <th>END TIME</th>
                            <th>CREATE AT</th>
                            <th>STATUS</th>
                            <th width="150"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($tasks->isNotEmpty())
                            @foreach ($tasks as $task)
                                <tr>
                                    <td>#{{ $task->id }}</td>
                                    <td>📒{{ $task->task }}</td>
                                    <td>🕔{{ $task->start_time }}</td>
                                    <td>🕦{{ $task->end_time }}</td>
                                    <td>📅{{ $task->created_at->format('M-D-Y') }}</td>
                                    <td>
                                        @switch($task->status)
                                            @case($task->status == 1)
                                                <span class="badge text-bg-secondary">⌛Waiting</span>
                                            @break

                                            @case($task->status == 2)
                                                <span class="badge text-bg-primary">⚙️Processing</span>
                                            @break

                                            @case($task->status == 3)
                                                <span class="badge text-bg-success">✅Complete</span>
                                            @break

                                            @case($task->status == 4)
                                                <span class="badge text-bg-danger">⛔Disable</span>
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        <a href="{{ route('task.show', $task->id) }}" class="btn btn-primary btn-sm">
                                            <i class="bi bi-eye"></i>
                                        </a>
                                        <a href="{{ route('task.edit', $task->id) }}" class="btn btn-success btn-sm">
                                            <i class="bi bi-pencil-square"></i>
                                        </a>
                                        <a class="btn btn-danger btn-sm" onclick="deleteTask({{ $task->id }})">
                                            <i class="bi bi-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7">Task not found</td>
                            </tr>
                        @endif

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>TASK</th>
                            <th>START TIME</th>
                            <th>END TIME</th>
                            <th>CREATE AT</th>
                            <th>STATUS</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#example').DataTable();
    </script>
    <script>
        function deleteTask(id) {
            Swal.fire({
                width: 400,
                height: 100,
                text: "Are you sure to delete this #" + id + "?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, delete it!",
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "get",
                        url: "/task/" + id + "/delete",
                        data: "data",
                        dataType: "json",
                        success: function(response) {
                            if (response['status'] == true) {
                                window.location.href = "{{ route('task.index') }}";
                            }
                        }
                    });
                }
            });
        }
    </script>
@endsection
