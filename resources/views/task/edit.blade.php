@extends('layout.app')

@section('content')
<div class="container">
    <div class="card">
        <form name="taskFormUpdate" id="taskFormUpdate" data-id = "{{ $task->id }}">
            <div class="card-header">
                Task ID #<span>{{ $task->id }}</span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col md-3">
                        <label for="starttime" class="form-label">start time⏱️</label>
                        <input type="time" class="form-control" name="start_time"id="start_timeUpdate" value="{{ $task->start_time }}">
                        <p></p>
                    </div>
                    <div class="col md-3">
                        <label for="starttime" class="form-label">end time⏱️</label>
                        <input type="time" class="form-control" name="end_time" id="end_timeUpdate" value="{{ $task->end_time }}">
                        <p></p>
                    </div>
                    <div class="col md-3">
                        <label for="starttime" class="form-label">status🪛</label>
                        <select class="form-select" name="status" id="statusUpdate">
                            <option value="1" {{ $task->status == 1 ? 'Selected' : '' }}> ⌛ - waiting</option>                                
                            <option value="2" {{ $task->status == 2 ? 'Selected' : '' }}> 🏃‍♂️ - processing</option>                           
                            <option value="3" {{ $task->status == 3 ? 'Selected' : '' }}> ✅ - complete</option>
                            <option value="4" {{ $task->status == 4 ? 'Selected' : '' }}> ⛔ - disable</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col md-12">
                        <label for="starttime" class="form-label">task📝</label>
                        <div class="form-floating">
                            <textarea class="form-control" placeholder="Leave a comment here" name="task" id="task_update">{{ $task->task }}</textarea>
                            <label for="floatingTextarea2">Comments</label>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-dark btn-sm" type="submit">Save change</button>
                <a href="{{ route('task.index') }}" class="btn btn-outline-dark btn-sm">Cancel</a>
            </div>
        </form>
    </div>
</div>
@endsection


@section('script')
<script>
$("#taskFormUpdate").on('submit', function (e) {
    var id = $(this).data('id');
    e.preventDefault();
    var element = $(this);
    var taskID = $(this).data('task');
    $.ajax({
        type: "post",
        url: "/task/" + id + "/update" ,
        data: element.serializeArray(),
        dataType: "json",
        success: function (response) {
            var error = response['errors'];
            if(response['status'] == false){
                if(error['task']){
                    $("#task_update")
                    .addClass('is-invalid')
                    .siblings('p')
                    .addClass('invalid-feedback').html(error['task']);
                }
                if(error['start_time']){
                    $("#start_timeUpdate")
                    .addClass('is-invalid')
                    .siblings('p')
                    .addClass('invalid-feedback').html(error['start_time']);
                }
                if(error['end_time']){
                    $("#end_timeUpdate")
                    .addClass('is-invalid')
                    .siblings('p')
                    .addClass('invalid-feedback').html(error['end_time']);
                }
            } 
            if(response['status'] == true){
                window.location.href = "{{ route('task.index') }}";
            }
            $(document).ready(function () {
                $('#start_timeUpdate').on('input', function() {
                    if ($(this).val().trim() !== '') {
                        $(this).removeClass('is-invalid');
                        $(this).addClass('is-valid');
                    }
                });
                $('#end_timeUpdate').on('input', function() {
                    if ($(this).val().trim() !== '') {
                        $(this).removeClass('is-invalid');
                        $(this).addClass('is-valid');
                    }
                });
                $('#task_update').on('input', function() {
                    if ($(this).val().trim() !== '') {
                        $(this).removeClass('is-invalid');
                        $(this).addClass('is-valid');
                    }
                });
            });
        }
    });
});
</script>
@endsection