@extends('layout.app')


@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Task ID #<span>{{ $task->id }}</span>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col md-3">
                    <label for="starttime" class="form-label">start time⏱️</label>
                    <input type="time" class="form-control" name="start_time"id="start_timeUpdate" value="{{ $task->start_time }}" disabled>
                    <p></p>
                </div>
                <div class="col md-3">
                    <label for="starttime" class="form-label">end time⏱️</label>
                    <input type="time" class="form-control" name="end_time" id="end_timeUpdate" value="{{ $task->end_time }}" disabled>
                    <p></p>
                </div>
                <div class="col md-3">
                    <label for="starttime" class="form-label">status🪛</label>
                    <select class="form-control" name="status" id="statusUpdate" disabled>
                        <option value="1" {{ $task->status == 1 ? 'Selected' : '' }}> ⌛ - waiting</option>                                
                        <option value="2" {{ $task->status == 2 ? 'Selected' : '' }}> 🏃‍♂️ - processing</option>                           
                        <option value="3" {{ $task->status == 3 ? 'Selected' : '' }}> ✅ - complete</option>
                        <option value="4" {{ $task->status == 4 ? 'Selected' : '' }}> ⛔ - disable</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col md-12">
                    <label for="starttime" class="form-label">task📝</label>
                    <div class="form-floating">
                        <textarea class="form-control" placeholder="Leave a comment here" name="task" id="task_update" disabled>{{ $task->task }}</textarea>
                        <label for="floatingTextarea2">Comments</label>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <a href="{{ route('task.index') }}" class="btn btn-outline-dark btn-sm">Back</a>
        </div>
    </div>
</div>

@endsection