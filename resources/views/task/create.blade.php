@extends('layout.app')

@section('content')
    <div class="container">
        @include('layout.message')
        <div class="card">
            <form name="taskForm" id="taskForm">
            <div class="card-header">
                New Task
            </div>
            <div class="card-body">
                    <div class="row">
                        <div class="col md-3">
                            <label for="starttime" class="form-label">start time⏱️</label>
                            <input type="time" class="form-control" name="start_time" id="start_time">
                            <p></p>
                        </div>
                        <div class="col md-3">
                            <label for="starttime" class="form-label">end time⏱️</label>
                            <input type="time" class="form-control" name="end_time" id="end_time">
                            <p></p>
                        </div>
                        <div class="col md-3">
                            <label for="starttime" class="form-label">status🪛</label>
                            <select class="form-select" name="status" id="status">
                                <option value="1"> ⌛ - waiting</option>
                                <option value="2"> 🏃‍♂️ - processing</option>
                                <option value="3"> ✅ - complete</option>
                                <option value="4"> ⛔ - disable</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col md-12">
                            <label for="starttime" class="form-label">task📝</label>
                            <div class="form-floating">
                                <textarea class="form-control" placeholder="Leave a comment here" name="task" id="task"></textarea>
                                <label for="floatingTextarea2">Comments</label>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-dark btn-sm" type="submit">Create</button>
                    <a href="{{ route('task.index') }}" class="btn btn-outline-dark btn-sm">Cancel</a>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
<script>
      $("#taskForm").on('submit', function (e) {
        e.preventDefault();
        var element = $(this);
        $.ajax({
            type: "post",
            url: "{{ route('task.store') }}",
            data: element.serializeArray(),
            dataType: "json",
            success: function (response) {
                var error = response['errors'];
                if(response['status'] == false){
                    if(error['task']){
                        $("#task")
                        .addClass('is-invalid')
                        .siblings('p')
                        .addClass('invalid-feedback').html(error['task']);
                    }
                    if(error['start_time']){
                        $("#start_time")
                        .addClass('is-invalid')
                        .siblings('p')
                        .addClass('invalid-feedback').html(error['start_time']);
                    }
                    if(error['end_time']){
                        $("#end_time")
                        .addClass('is-invalid')
                        .siblings('p')
                        .addClass('invalid-feedback').html(error['end_time']);
                    }
                } 
                if(response['status'] == true){
                    window.location.href = "{{ route('task.index') }}";
                }
                $(document).ready(function () {
                    $('#start_time').on('input', function() {
                        if ($(this).val().trim() !== '') {
                            $(this).removeClass('is-invalid');
                            $(this).addClass('is-valid');
                        }
                    });
                    $('#end_time').on('input', function() {
                        if ($(this).val().trim() !== '') {
                            $(this).removeClass('is-invalid');
                            $(this).addClass('is-valid');
                        }
                    });
                    $('#task').on('input', function() {
                        if ($(this).val().trim() !== '') {
                            $(this).removeClass('is-invalid');
                            $(this).addClass('is-valid');
                        }
                    });
                });
            }
        });
    });
</script>
@endsection