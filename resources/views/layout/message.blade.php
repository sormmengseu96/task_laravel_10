@if (Session::has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong></strong> {{ Session::get('success') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif
@if (Session::has('inform'))
<div class="alert alert-info alert-dismissible fade show" role="alert">
    <strong></strong> {{ Session::get('inform') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif
@if (Session::has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong></strong> {{ Session::get('danger') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif
